`timescale 1ns / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:01:22 07/18/2020 
// Design Name: 
// Module Name:    rx_unit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rx_unit(
    input clk,
    input RxD,
    output reg RxD_data_ready,
    output reg [7:0] RxD_data
    );


parameter ClkFrequency = 24000000;
parameter Baud = 115200;
parameter Oversampling = 4;	// needs to be a power of 2


// BaudTick contol unit parameters
parameter IDLE = 3'b000;
parameter start = 3'b001;
parameter counting = 3'b010;
parameter storing = 3'b011;
parameter stop_bit = 3'b100;
parameter data_ready = 3'b101;

reg [2:0] RxD_state;
reg [2:0] state_reg;
reg [1:0] count;
reg [2:0] ready_count ;
reg [1:0] sum;

wire BaudTick_Baud;
reg tick_en;
initial begin 
	tick_en = 1'b1;
	RxD_state = IDLE;
	state_reg = IDLE;
	RxD_data_ready = 1'b0;
   count = 2'b00;
   ready_count = 3'b000;
   sum = 2'b00;
end
baud_tick_generator #(ClkFrequency, Baud, Oversampling) tickgen(.clk(clk), .enable(tick_en), .tick(BaudTick_Baud));

always @(posedge BaudTick_Baud) begin
    case (RxD_state)
      IDLE: begin         // Wait until RxD become 0 (start)
        if(RxD == 1'b0) begin
          RxD_state <= start;
        end
        else
          RxD_state <= IDLE;
      end

      start: begin        //stay 3 clocks at start (Rxd==0)
        if(count >= 2'b11)
          RxD_state <= counting;
        else if(RxD == 1'b1)
          RxD_state <= IDLE;
        else 
          RxD_state <= start;
      end

      counting: begin
        if(count >= 2'b11)
          RxD_state <= storing;
        else
          RxD_state <= counting;
      end

      storing: begin
        if(ready_count == 3'b111)
          RxD_state <= stop_bit;
        else
          RxD_state <= counting;
      end
      stop_bit: RxD_state <= data_ready;

      data_ready: begin
        RxD_state <= IDLE;
       // RxD_data_ready <= 1'b1;
      end

      default: RxD_state <= IDLE;
    endcase
end



always @(posedge clk) begin
    state_reg <= RxD_state;
    if(RxD_data_ready == 1'b1)
      RxD_data_ready <= 1'b0;
    else if(RxD_state == data_ready && state_reg != data_ready)
      RxD_data_ready <= 1'b1;
    else
      RxD_data_ready <= 1'b0;
end

always @(posedge BaudTick_Baud) begin
    if(RxD_state == IDLE) begin
      count <= 2'b00;
      ready_count <= 3'b000;
      sum <= 2'b00;
    end
    else begin
      count <= count + 2'd1;
      if(count == 2'b00)
        sum <= 2'b00;
      else
        sum <= sum + RxD;
      if(RxD_state == storing) begin
        RxD_data[ready_count] <= sum[1];
        ready_count <= ready_count + 3'd1;
      end
    end
end


endmodule
