`timescale 1ns / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:54:55 07/18/2020 
// Design Name: 
// Module Name:    baud_tick_generator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module baud_tick_generator(
    input clk,
    input enable,
    output reg tick
    );
parameter ClkFrequency = 24000000;
parameter Baud = 115200;
parameter Oversampling = 1;


parameter BaudTick = ClkFrequency / (Baud * Oversampling);
parameter LEN = $clog2(BaudTick);
reg[LEN-1:0] counter;

initial begin
	counter = {(LEN){1'b0}};
end


always@(posedge clk) begin
	if (enable) begin
		counter <= counter + 1'b1;

		if (counter == BaudTick-1) begin
			counter <= {(LEN){1'b0}};
			tick <= 1'b1;
		end
		else tick <= 1'b0;
	end
end

endmodule
