`timescale 1ns / 1ns

module fir(clk, rst, FIR_input, FIR_input_ready, FIR_output, FIR_output_ready);
	// Sizes
	parameter WIDTH1 = 16;
	parameter WIDTH2 = 38;
	
	// Arguments
	input clk, rst, FIR_input_ready;
	input[WIDTH1 - 1:0] FIR_input;
	output FIR_output_ready;
	output[WIDTH2 - 1:0] FIR_output;

	// States
	parameter IDLE    =   3'b000;
	parameter LOAD    =   3'b001;
	parameter READY   =   3'b010;
	parameter CALC    =   3'b011;
	parameter DOUT    =   3'b100;
	parameter WAIT    =   3'b101;
	
	// Values
	parameter ONE     =   1'b1;
	parameter ZERO    =   1'b0;


	reg[2:0]  present_state;

	
	reg                d_ready;	
	reg                rst_acc;
	reg [5:0]          counter = 6'b0;
	reg [5:0]          counter_rom = 6'b0;
	wire[WIDTH1 - 1:0] rom_out, shift_out;
	wire[WIDTH2 - 1:0] mult_out;
	wire[WIDTH2 - 1:0] acc_out;
	reg        out_ready;
	reg        acc_en;

	initial begin
		present_state = 3'b0;
		counter = 6'b0;	
		counter_rom = 6'b0;
		out_ready = ZERO;
		acc_en = ZERO;
	end

	// Main modules	
rom rom1(
  .clka(clk),
  .addra(counter_rom),
  .douta(rom_out)
);
	//ROM rom1(.select(counter),  .selected_value(rom_out));
	multiplier mult(.num1(shift_out), .num2(rom_out), .multiplication(mult_out));
	shiftRegister shift_register(.clk(clk), .rst(rst), .data_ready(d_ready), .in_data(FIR_input), .index(counter), .out(shift_out));	
	accumulator acc(.clk(clk), .rst(rst_acc), .enable(acc_en), .inputValue(mult_out), .cumulativeSum(acc_out));

	always @ (posedge clk) begin
		case(present_state)
			IDLE:begin
				if (FIR_input_ready == ONE) begin
					present_state <= LOAD;
					d_ready <= ONE;
 
				end
				else begin
					present_state <= IDLE;
				end
				acc_en  <= ZERO; 
				rst_acc <= ONE; 
			end
			LOAD:begin 
				present_state <= READY; 
				d_ready       <= ZERO; 
				rst_acc       <= ONE;
				acc_en        <= ZERO;  
				out_ready     <= ZERO; 
				counter       <= 6'b0;
				counter_rom   <= 6'b0; 
			end
			READY:begin 
				present_state <= CALC;
				acc_en        <= ONE;
				rst_acc       <= ZERO;
				d_ready       <= ZERO;
				out_ready     <= ZERO; 
				counter_rom   <= counter_rom + 1'b1;
				 
			end
			CALC:begin 
				if (counter < 6'b111111) begin 
					present_state <= CALC;
					counter       <= counter + 1'b1;
					counter_rom   <= counter_rom + 1'b1; 
				end
				else begin
					present_state <= DOUT;
					d_ready       <= ZERO;
					acc_en        <= ZERO;
					
				end  
			end 
			DOUT:begin 
				present_state <= WAIT;
				out_ready     <= ONE;
				
			end
			WAIT:begin
				present_state <= IDLE;
				rst_acc       <= ONE; 
				out_ready     <= ZERO;
			end
		endcase
	end
	assign FIR_output = (out_ready == ONE)?acc_out:38'b0;
	assign FIR_output_ready = out_ready;


endmodule

//module ROM(select, selected_value);
//	parameter WIDTH = 16;
//	parameter LENGTH = 64;
//	parameter WIDTH_SELECT = 6;
//	
//	input[WIDTH_SELECT - 1:0] select;
//	output[WIDTH - 1:0] selected_value;
//
//	reg signed [WIDTH - 1:0] coeffs [0:LENGTH-1];
//	assign selected_value = coeffs[select];
//endmodule



module shiftRegister(clk, rst, data_ready, in_data, index, out);
	parameter WIDTH = 16;
	parameter LENGTH = 64;
	parameter WIDTH_SELECT = 6;
	
	input clk, rst, data_ready;
	input[WIDTH - 1:0] in_data;
	input[WIDTH_SELECT - 1:0] index;
	output[WIDTH - 1:0] out;
	
	reg[WIDTH - 1:0] memory[0:LENGTH - 1];

	integer i;
	initial begin
		for(i = 0 ; i < LENGTH ; i = i + 1)  begin
			memory[i] = 16'b0;
		end
		i = 0;
	end

	always @ (posedge clk) begin
		if (data_ready) begin
			for (i=0; i < LENGTH - 1; i=i+1)
				memory[i+1] <= memory[i];
			memory[0] <= in_data;
		end
			
	end
	assign out = memory[index]; 
endmodule

module multiplier(num1, num2, multiplication);
	parameter WIDTH_IN  = 16;
	parameter WIDTH_OUT = 38;
	input signed[WIDTH_IN - 1:0] num1, num2;
	output[WIDTH_OUT - 1:0] multiplication; 

	wire signed[WIDTH_OUT - 1:0] res;
	assign res = num1 * num2;
	assign multiplication = res;		
endmodule



module accumulator(clk, rst, enable, inputValue, cumulativeSum);
	parameter WIDTH = 38;
	input clk, rst, enable;
	input[WIDTH - 1:0] inputValue;
	output[WIDTH - 1:0] cumulativeSum;

	reg[WIDTH - 1:0] internal_storage;	
	initial begin
		internal_storage = 38'b0;
	end
	always @(posedge clk)begin
		if(rst) internal_storage <= 38'b0;
		if (enable == 1'b1) internal_storage <= internal_storage + inputValue;	
	end
	assign cumulativeSum = internal_storage;	
endmodule


