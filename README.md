# FIR Filter
### Introduction
This project is a part of FPGA course at University of Tehran.<br>
In this project, a fir low-pass discrete filter is implemented that is used to filter high frequency noise in audio.
Audio samples are sent to fpga using implemented UART module as 2 8bits data and then filtered result will send back to host.

### Prerequisites
This code is written in verilog. Xilinx ISE 14.7 is used to implement modules and simulate them.


### FIR Filter
This filter is designed in matlab, exported filter's coeeficients are used in ROM ipcore that is a part of fir module.
FIR module is designed in fully serial manner, which means only one adder and multiplyer is used.
It takes 64 cycles to calculate filter result and totally takes 68 clock cycles. Mult and Adder are shared between all 64 calculation cycles.