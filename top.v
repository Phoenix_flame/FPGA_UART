`timescale 1ns / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Phoenix_flame
// 
// Create Date:    11:48:06 07/19/2020 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
    input clk_i,
	 input rst_i,
	 input rx_i,
    output tx_o,
	 output[1:0] led_o
    );

parameter RX_1 = 2'b00;
parameter RX_2 = 2'b01;
parameter RX_3 = 2'b10;
parameter RX_4 = 2'b11;

parameter TX_1 = 3'd0;
parameter TX_11 = 3'd1;
parameter TX_2 = 3'd2;
parameter TX_3 = 3'd3;
parameter TX_4 = 3'd4;

reg txd_start;
wire txd_busy;
reg[1:0] state_RX;
reg[2:0] state_TX;

wire data_in_ready;
wire[7:0] rx_data_out;
reg[7:0] txd_data_in;
reg[15:0] inData;
reg send_data;
reg fir_input_ready;
wire [37:0] fir_out;
reg [37:0] buffer;
wire fir_out_ready;
initial begin 
	buffer = 38'b0;
	txd_start = 1'b0;
	state_RX = 2'b0;
	state_TX = 3'b0;
	inData = 16'b0 ; 
	send_data = 1'b0;
	fir_input_ready = 1'b0;
end

rx_unit receiver(.clk(clk_i), .RxD(rx_i), .RxD_data_ready(data_in_ready), .RxD_data(rx_data_out));
tx_unit sender(.clk(clk_i), .TxD_start(txd_start), .TxD_data(txd_data_in), .TxD(tx_o), .TxD_busy(txd_busy));
fir filter(.clk(clk_i), .rst(rst_i), .FIR_input(inData), .FIR_input_ready(fir_input_ready), .FIR_output(fir_out), .FIR_output_ready(fir_out_ready));

always @(posedge clk_i) begin
	case (state_RX)
		RX_1: begin
			send_data <= 1'b0;
			if (data_in_ready)begin 
				state_RX <= RX_4;
				inData[15:8] <= rx_data_out;
			end
			else state_RX <= RX_1;
		end
		RX_4: begin
			if (~data_in_ready) begin
				state_RX <= RX_2;
			end
		end 
		RX_2: begin
		   if (data_in_ready)begin 
				state_RX <= RX_3;
				inData[7:0] <= rx_data_out;
				fir_input_ready <= 1'b1;
			end
			else state_RX <= RX_2;
		end
		RX_3: begin
			
			if (fir_out_ready)begin
				state_RX <= RX_1;
				fir_input_ready <= 1'b0;
				buffer <= fir_out;
				send_data <= 1'b1;
			end
			
			
		end

		default: state_RX <= RX_1;
	endcase
end

always @(posedge clk_i)begin

		if(state_TX == TX_1)begin
			if (send_data)begin
				
				txd_data_in <= buffer[30:23];
				txd_start <= 1'b1;
				state_TX <= TX_11;
			end
			else state_TX <= TX_1;
		end
		else if(state_TX == TX_11)begin
			state_TX <= TX_2;
		end
		else if(state_TX == TX_2)begin
			txd_start <= 1'b0;
			if (~txd_busy) begin
				txd_data_in <= buffer[22:15];
				txd_start <= 1'b1;
				state_TX <= TX_3;
			end
			else state_TX <= TX_2;
		
		end
		else if(state_TX == TX_3)begin
			txd_start <= 1'b0;
			state_TX <= TX_4;
		
		end
		else if(state_TX == TX_4)begin
			if (~txd_busy) begin
				state_TX <= TX_1;
			end 
			else state_TX <= TX_4;
		end
end

assign led_o = state_RX;


endmodule
