`timescale 1ns/1ns

module TB();

reg clk, rst;
wire rx;
wire tx;

reg txd_start;
reg [7:0] txd_data;
wire txd_busy;
wire [1:0] led;
initial begin 
	clk = 1'b0;
	rst = 1'b0;
	txd_start = 1'b0;
	txd_data = 8'h01;
end



tx_unit sender(.clk(clk), .TxD_start(txd_start), .TxD_data(txd_data), .TxD(rx), .TxD_busy(txd_busy));


top uut(
  .clk_i(clk),
  .rx_i(rx),
  .tx_o(tx),
  .led_o(led)
);

always #1 clk = ~clk;

initial begin
	#100 txd_start = 1'b1;
	#20 txd_start = 1'b0;
	txd_data = 8'h25;
	@(negedge txd_busy)begin
		#100
		txd_start = 1'b1;
		#20
		txd_start = 1'b0;
	end
	txd_data = 8'h0e;
	@(negedge txd_busy)begin
		#100
		txd_start = 1'b1;
		#20
		txd_start = 1'b0;
	end
	txd_data = 8'h11;
	@(negedge txd_busy)begin
		#100
		txd_start = 1'b1;
		#20
		txd_start = 1'b0;
	end
	
	
end






endmodule