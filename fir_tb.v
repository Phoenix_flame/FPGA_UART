`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:06:18 08/09/2020 
// Design Name: 
// Module Name:    fir_tb 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module fir_tb(
    );
parameter             IN_WIDTH = 16;
parameter             OUT_WIDTH = 38;
parameter             DATA_LEN = 221184;
reg   [IN_WIDTH-1:0]  din;
wire  [OUT_WIDTH-1:0] dout;
reg   [OUT_WIDTH-1:0] ex_out;
reg   [IN_WIDTH-1:0]  input_data [0:DATA_LEN - 1];
reg   [OUT_WIDTH-1:0] expected_data [0:DATA_LEN - 1];
reg                   clk;
reg                   rst;
wire                  ready;
reg                   input_ready;
integer fp,cnt,k;

fir uut (
	.clk(clk), 
	.rst(rst),
	.FIR_input_ready(input_ready), 
	.FIR_input(din), 
	.FIR_output_ready(ready), 
	.FIR_output(dout)
);





initial
    begin  
    $readmemb("inputs.txt", input_data);   
end

initial
    begin
    $readmemb("outputs.txt", expected_data);
end   


// initial begin
// $dumpfile("waves.vcd");
// $dumpvars(0, uut);
// $dumpvars(0, uut.acc);
// $dumpvars(0, uut.shift_register);
// end
 

         
//------- Clock ---------//
// Initialization
initial
begin
	clk = 1'b0;
end

// Clock generator
always #10 clk = ~clk;
//-----------------------//

// reset whole module
initial
   begin  
	din = input_data[0];
	cnt=0;
end   

       
integer j;
integer i;
initial
begin
input_ready = 1'b0;
	i = 0;
	#410 din = input_data[0];	
	fp = $fopen("outManualFIRVerilog.txt");
	$display("Testing %d Samples...",DATA_LEN);	

	for(j = 1 ; j <= (DATA_LEN + 1) * 67; j = j + 1)begin
		ex_out = expected_data[i];
		input_ready = 1'b1;
		@(posedge ready) begin
				#2
			   input_ready = 1'b0;
				if(ex_out != dout)begin
					$display("test failed: %d   input: %b expected: %b output: %b" , i, din, ex_out, dout);
					$fwrite(fp, "Incorrect output\n");
					$finish;
				end
				i = i + 1;	
				$fwrite(fp,"%b\n",dout);
				$display("correct: %d   input: %b expected: %b output: %b" , i, din, ex_out, dout);
				din = input_data[i];
		end


	end
	$display("test passed");	
	$stop;
end

endmodule
